import 'dart:convert';
import 'package:http/http.dart' as http;

class User {
  String id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  User({this.id,this.email,this.firstName, this.lastName,this.avatar});
  factory User.createUser(Map<String, dynamic> object) {
    return User(
      id: object["id"].toString(),
      email: object["email"],
      firstName: object["first_name"],
      lastName: object["last_name"],
      avatar: object["avatar"]

    );
  }
  static Future<User> getUserFromApi(int id)async{
    String apiurl = "https://reqres.in/api/users/"+id.toString();

    var apiResult = await http.get(apiurl);
    var jsonobject = json.decode(apiResult.body);
    var userdata = (jsonobject as Map<String,dynamic>) ['data'];
    return User.createUser(userdata);
  }
  
}
class UninitializedUser extends User {
  
}